﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2.Models
{
    public class Account
    {
        [Required]
        public int AccountID { get; set; }
        [StringLength(100,ErrorMessage="Chỉ nhập tối đa 100 ký tự")]
        public String FirstName { get; set; }
        [StringLength(100, ErrorMessage = "Chỉ nhập tối đa 100 ký tự")]
        public String LastName { get; set; }
        [DataType(DataType.Password)]
        public String Password { get; set; }
        [DataType(DataType.EmailAddress)]
        public String Email { get; set; }

        public virtual ICollection<Post> Posts { get; set; }


    }
}