﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog_2.Models
{
    public class Post
    {
        [Required]
        public int ID{get;set;}
        [StringLength(500, ErrorMessage = "Độ dài ký tự cho phép 20 - 500", MinimumLength = 20)]
        public String Title { get; set; }
         [StringLength(1000000000, ErrorMessage = "Tối thiểu 50 ký tự", MinimumLength = 50)]
        public String Body { get; set; }
        [DataType(DataType.DateTime, ErrorMessageResourceName = "Phải nhập đúng định dạng", ErrorMessage = "Phải nhập đúng định dạng")]
        public DateTime DateCreated { get; set; }
        [DataType(DataType.DateTime, ErrorMessageResourceName = "Phải nhập đúng định dạng", ErrorMessage = "Phải nhập đúng định dạng")]
        public DateTime DateUpdated { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }

        public int AccountID { get; set; }
        public virtual Account Accounts { get; set; }

    }
}