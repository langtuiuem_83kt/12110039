﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2.Models
{
    public class Tag
    {
        [Required]
        public int TagID { get; set; }
        [StringLength(100,ErrorMessage = "Bạn chỉ được phép nhập từ 10 - 100 ký tự", MinimumLength = 10)]
        public String Content { get; set; }

        public virtual ICollection<Post> Posts { get; set; }
    }
}