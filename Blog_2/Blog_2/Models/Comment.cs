﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_2.Models
{
    public class Comment
    {
        public int ID { get; set; }
        [Required]
        [StringLength(500,ErrorMessage="Độ dài ký tự cho phép 20 - 500", MinimumLength= 20)]
        public String Title { get; set; }
        [StringLength(1000000000, ErrorMessage = "Tối thiểu 50 ký tự", MinimumLength = 50)]
        public String Body { get; set; }
        [DataType(DataType.DateTime, ErrorMessageResourceName = "Phải nhập đúng định dạng", ErrorMessage = "Phải nhập đúng định dạng")]
        public DateTime DateCreated { get; set; }
        [DataType(DataType.DateTime, ErrorMessageResourceName = "Phải nhập đúng định dạng", ErrorMessage = "Phải nhập đúng định dạng")]
        public DateTime DateUpdated { get; set; }
        public String Author { get; set; }

        public int PostID { get; set; }
        public virtual Post Post { get; set; }
    }
}