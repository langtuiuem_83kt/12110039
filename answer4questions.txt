1. Lợi ích mà website mang lại:
- Giúp các Thế Giới Gamer Liên Minh Huyền Thoại (LOL) có thể kết nối với nhau như 1 gia đình.
- Giúp Gamer LOL hiểu rỏ được bản chất của tướng mình chọn, giúp cho các gamer đến được với thế giới
của các gamer chuyên nghiệp.
- Tạo được môi trường chơi game lành mạnh, không chửi tục văng tục trong game.
- Giúp cho các bạn hong phải là gamer hiểu rõ được gamer hong phải là nơi hư đốn như mọi người thường tưởng tượng
mà nó là 1 loại hình E-Sport giúp giải trí xã stress và là nơi thi đấu như các loại hình thể thao khác.
2. Sự khác biệt so với các website cùng loại (nêu tên và ghi lại sự khác biệt)
- Tập hợp các cách lên đồ của các trang web uy tín. Không còn rời rạc như các trang web hiện nay.
- Cập nhật tin tức, tướng, phiên bản game 1 thường xuyên và được hiện rõ dể nắm bắt.
- Trang web được thiết kế đơn giản hết mức để người sử dụng dể dàng thực hiện những điều mình muốn.
- Tạo được mối liên kết giữa các gamer, làm cho thế giới game trở nên mạnh mẻ và được mọi người chấp nhận nó là 1
loại hình E-Sport. chứ không phải là thứ hủy hoại tương lai con người.
- Có video đi kèm, làm cho trang web trở nên sinh động hơn về cách thức truyền đạt thông tin đến người dùng.
3. Nếu không sử dụng website của bạn thì người dùng mất gì?
- Không nắm bắt kịp được những cách lên đồ cần thiết của tướng hoặc có bắt kịp thì cũng qua loa.
- Không thể kết nối thế giới gamer, bạn sẽ bị lạc lõng trong thế giới game (vì hiện nay việc "troll" game rất nhiều
mà điều đo xảy ra là do bạn chưa hiểu rõ được cách chơi)
- Sẽ cảm thấy chán nãn khi chơi game và từ đó trở nên bực tức, bắt đầu bỏ tiền thuê người "cày" giúp.
4. Website của bạn là vitamin hay PainKiller?
- Website này là vừa có chút vitamin vừa có chút PainKiller.
+ Vitamin: 
. Giúp cho người chơi trút bỏ bực tức khi chơi game (trong phần comment), tìm được những đồng đội hợp ý mình
để cùng chơi.
. Cũng có thể giúp tạo nên những mối tình các FA boy gamer và FA girl gamer. Làm cho thế giới game trở nên ngọt ngào hơn
+ Painkiller:
. Khi con người chúng ta có 1 nổi đâu nào đó thường tìm những việc nặng nhọc để vơi đi nổi đâu ấy. Điều này tốt nhưng việc
làm hại sức khỏe như thế thì không được tốt. Khi đến với Game đến với website bạn có thể chia sẻ kỹ năng, kih nghiệm chơi game bạn
có, cũng như cũng có thể chia sẽ nổi đau đó để cho gia đình gamer cùng cảm nhận. Và khi trút được nổi buồn vào 1 trận game đỉnh cao
thì còn gì bẳng. Mọi buồn phiền sẽ tự tan biến mất. Bạn trở nên vui vẻ khi được vánh vai cùng với 1 đồng đội ăn ý.